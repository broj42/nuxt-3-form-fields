export const useFormFieldProps = () => {
  return {
    modelValue: {},
    id: {
      type: [String, Number]
    },
    name: {
      type: [String, Number]
    },
    label: {
      type: String
    },
    errors: {
      type: [Array, String]
    },
    info: {
      type: String
    },
    inline: {
      type: Boolean,
      default: false
    },
    tooltip: {
      type: [String, Boolean],
      default: false
    },
    tooltipClickOnly: {
      type: Boolean,
      default: false
    },
    validate: {
      type: Object
    },
    disabled: {
      type: Boolean,
      default: false
    },
    required: {
      type: Boolean,
      default: false
    }
  }
}

export const useFormField = (props, emit) => {
  let validateTimeout = null

  // createUniqueID from utils/index.js
  const uniqueID = props.id || ref(createUniqueID(10))
  const errorList = ref([])
  const hasFocus = ref(false)

  const formLabelProps = computed(() => {
    return {
      forID: uniqueID.value,
      label: props.label,
      tooltip: props.tooltip,
      hasTooltip: hasTooltip.value,
      tooltipClickOnly: props.tooltipClickOnly
    }
  })

  const isDisabled = computed(() => {
    return props.disabled !== false
  })

  const isRequired = computed(() => {
    return props.required !== false
  })

  const isError = computed(() => {
    return mergedErrors.value?.length > 0
  })

  const isInline = computed(() => {
    return props.inline !== false
  })

  const hasTooltip = computed(() => {
    return props.tooltip !== false && props.tooltip !== ''
  })

  const hasValue = computed(() => {
    return (
      ![null, undefined].includes(fieldValue.value) &&
      (fieldValue.value.toString().length > 0 || fieldValue.value.toString() !== '')
    )
  })

  const mergedErrors = computed(() => {
    const errors = errorList.value ? [...errorList.value] : []
    if (props.errors) {
      const e = !Array.isArray(props.errors) ? [props.errors] : props.errors
      errors.unshift(...e)
    }

    return errors
  })

  const setFocus = state => {
    hasFocus.value = state
  }

  const fieldValue = computed({
    get() {
      return props.modelValue
    },

    set(value) {
      emit('update:modelValue', value)
    }
  })

  watch(fieldValue, () => {
    if (props.validate) {
      clearTimeout(validateTimeout)
      validateTimeout = setTimeout(
        $validate(props.validate, props.modelValue, { ...props, errorList, fieldValue }),
        500
      )
    }
  })

  return {
    uniqueID,
    fieldValue,
    isError,
    isInline,
    isDisabled,
    isRequired,
    hasValue,
    hasFocus,
    hasTooltip,
    mergedErrors,
    formLabelProps,
    setFocus
  }
}
