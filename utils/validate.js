const getMessage = (message, type) => {
  if (message === undefined) return undefined
  if (typeof message === 'string') return message
  else return message[type]
}

const validate = {
  required(value, attributes, instance) {
    return {
      isError: [undefined, null].includes(value) || value.length === 0 || value === '',
      message: getMessage(attributes.message, 'required') || 'Ovo polje je obavezno'
    }
  },

  number(value, attributes, instance) {
    if (attributes.strict) {
      value = parseInt(value.toString().replace(/[^\d]/g, ''))
      instance.fieldValue.value = !isNaN(value) ? value : ''
    } else {
      return {
        isError: !/^\d*$/.test(value),
        message: getMessage(attributes.message, 'number') || 'Must be a number'
      }
    }
  },

  phone(value, attributes, instance) {
    if (attributes.strict) instance.fieldValue.value = value.replace(/[^\+\d]/g, '')
    else {
      return {
        isError: !/[^\+\d]*$/.test(value),
        message: getMessage(attributes.message, 'phone') || 'Must be a phone number'
      }
    }
  },

  decimal(value, attributes, instance) {
    if (attributes.strict) {
      instance.fieldValue.value = value.toString().replace(/[^\d|\.]/g, '')
      if (instance.fieldValue.value.split('.').length - 1 > 1) instance.fieldValue.value = value.slice(0, -1)
    }
    return {
      isError: !/^(?!(0\d))\d+\.\d+$/.test(value),
      message: getMessage(attributes.message, 'decimal') || 'Must be a decimal number'
    }
  },

  email(value, attributes, instance) {
    return {
      isError:
        !/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        ),
      message: getMessage(attributes.message, 'email') || 'Molimo unesite ispravnu e-mail adresu'
    }
  },

  minlength(value, attributes, instance) {
    let minlength = parseInt(attributes.minlength || instance.minlength)
    value = Array.isArray(value) ? value : value.toString()
    return {
      isError: value.length < minlength,
      message: getMessage(attributes.message, 'minlength') || `Min length is ${minlength}`
    }
  },

  maxlength(value, attributes, instance) {
    let maxlength = parseInt(attributes.maxlength || instance.maxlength)
    value = Array.isArray(value) ? value : value.toString()
    if (attributes.strict) instance.fieldValue.value = instance.fieldValue.value.slice(0, maxlength)
    else {
      return {
        isError: value.length > maxlength,
        message: getMessage(attributes.message, 'maxlength') || `Max length is ${maxlength}`
      }
    }
  },

  min(value, attributes, instance) {
    let min = parseInt(attributes.min || instance.min)
    return {
      isError: value < min,
      message: getMessage(attributes.message, 'min') || `Number must be larger than ${min} or equal`
    }
  },

  max(value, attributes, instance) {
    let max = parseInt(attributes.max || instance.max)
    return {
      isError: value > max,
      message: getMessage(attributes.message, 'max') || `Number must be larger than ${max} or equal`
    }
  },

  pattern(value, attributes, instance) {
    let pattern = ''
    for (let p of attributes.pattern) {
      if (/^((?![`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).)*$/.test(p)) pattern += '.'
      else pattern += '\\' + p
    }
    return {
      isError: !new RegExp(pattern + '$').test(value),
      message: getMessage(attributes.message, 'pattern') || `Pattern must be ${attributes.pattern}`
    }
  },

  date(value, attributes, instance) {
    let dateHelper = value.split(attributes.delimiter)
    let formatHelper = attributes.pattern.split(attributes.delimiter)
    let day = dateHelper[formatHelper.findIndex(i => i.toLowerCase().includes('d'))]
    let month = dateHelper[formatHelper.findIndex(i => i.toLowerCase().includes('m'))]
    let year = dateHelper[formatHelper.findIndex(i => i.toLowerCase().includes('y'))]
    return {
      isError:
        [day, month, year].some(i => i === undefined || i.length === 0) || !Date.parse(`${month}-${day}-${year}`),
      message: getMessage(attributes.message, 'date') || `Date is not valid`
    }
  },

  //Fix
  checked(value, attributes, instance) {
    return {
      isError: !instance.isChecked,
      message: getMessage(attributes.message, 'checked') || `Ovo polje je obavezno`
    }
  }
}

export const $validate = (attributes, value, instance) => {
  const response = []
  if (typeof attributes.type === 'string') attributes.type = [attributes.type]
  for (let t of attributes.type) {
    if (typeof t === 'string') {
      const e = validate[t](value, attributes, instance)
      if (e && e.isError) response.push(validate[t](value, attributes, instance))
    } else {
      const isError = !Object.values(t)[0](value)
      const key = Object.keys(t)[0]
      if (isError && attributes.message && attributes.message[key]) {
        response.push({
          isError,
          message: attributes.message[key]
        })
      }
    }
  }

  if (response.length > 0) instance.errorList.value = response.map(i => i.message)
  else if (instance?.errorList?.value?.length > 0) instance.errorList.value = undefined
}